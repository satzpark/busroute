package com.busroute.busroute.bean;

import android.support.v7.widget.RecyclerView;

/**
 * Created by Bewo Technologies on 10/23/2017.
 */

public class BusRouteBean {
    String str_BusRoute;
    String str_ID;
    String[] strA_Stops;
    String str_Desc;
    boolean bool_Accessible;
    String str_ImageURL;

    public String getStr_BusRoute() {
        return str_BusRoute;
    }

    public void setStr_BusRoute(String str_BusRoute) {
        this.str_BusRoute = str_BusRoute;
    }

    public String getStr_ID() {
        return str_ID;
    }

    public void setStr_ID(String str_ID) {
        this.str_ID = str_ID;
    }

    public String[] getStrA_Stops() {
        return strA_Stops;
    }

    public void setStrA_Stops(String[] strA_Stops) {
        this.strA_Stops = strA_Stops;
    }

    public String getStr_Desc() {
        return str_Desc;
    }

    public void setStr_Desc(String str_Desc) {
        this.str_Desc = str_Desc;
    }

    public boolean isBool_Accessible() {
        return bool_Accessible;
    }

    public void setBool_Accessible(boolean bool_Accessible) {
        this.bool_Accessible = bool_Accessible;
    }

    public String getStr_ImageURL() {
        return str_ImageURL;
    }

    public void setStr_ImageURL(String str_ImageURL) {
        this.str_ImageURL = str_ImageURL;
    }
}
