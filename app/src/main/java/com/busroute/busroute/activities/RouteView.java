package com.busroute.busroute.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.busroute.busroute.R;
import com.busroute.busroute.bean.BusRouteBean;
import com.squareup.picasso.Picasso;

import java.util.Arrays;

public class RouteView extends AppCompatActivity {

    TextView tvRoute;
    TextView tvDesc;
    Button btnAccessible;
    ImageView ivRoute;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_route_view);
        int i= getIntent().getExtras().getInt("busroute");
        BusRouteBean busRouteBean= MainActivity.mList.get(i);
        tvRoute= (TextView) findViewById(R.id.tv_BusRouteName);
        tvDesc= (TextView) findViewById(R.id.tv_Desc);
        btnAccessible= (Button) findViewById(R.id.btn_Accessible);
        ivRoute= (ImageView) findViewById(R.id.iv_Route);

        tvRoute.setText(busRouteBean.getStr_BusRoute());
        tvDesc.setText(busRouteBean.getStr_Desc());
        Picasso.with(getBaseContext()).load(busRouteBean.getStr_ImageURL()).into(ivRoute);
        if(busRouteBean.isBool_Accessible()){
            btnAccessible.setVisibility(View.VISIBLE);
        }
        for (int j=0;j<busRouteBean.getStrA_Stops().length;j++)
        Log.d("Route View", busRouteBean.getStrA_Stops()[j]);

    }
}
