package com.busroute.busroute.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.busroute.busroute.activities.CustomJSONObjectRequest;
import com.busroute.busroute.R;
import com.busroute.busroute.adapter.BusRouteAdapter;
import com.busroute.busroute.bean.BusRouteBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements Response.Listener,
        Response.ErrorListener {
    RecyclerView rv_BusRoute;
    public static final String REQUEST_TAG = "MainVolleyActivity";
    private RequestQueue mQueue;
    BusRouteAdapter busRouteAdapter;
    public static List<BusRouteBean> mList=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv_BusRoute=(RecyclerView) findViewById(R.id.rv_busroute);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getBaseContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rv_BusRoute.setLayoutManager(mLayoutManager);
        rv_BusRoute.addItemDecoration(new DividerItemDecoration(getBaseContext(), LinearLayoutManager.VERTICAL));
        rv_BusRoute.setItemAnimator(new DefaultItemAnimator());
    }
    @Override
    protected void onStart() {
        super.onStart();
        // Instantiate the RequestQueue.
        mQueue = CustomVolleyRequestQueue.getInstance(this.getApplicationContext())
                .getRequestQueue();
        String url = "http://www.mocky.io/v2/5808f00d10000005074c6340";
        final CustomJSONObjectRequest jsonRequest = new CustomJSONObjectRequest(Request.Method
                .GET, url,
                new JSONObject(), this, this);
        jsonRequest.setTag(REQUEST_TAG);
        mQueue.add(jsonRequest);

    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mQueue != null) {
            mQueue.cancelAll(REQUEST_TAG);
        }
    }
    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(Object response) {
       // Log.d(REQUEST_TAG,response.toString());
        if(response!=null){
            try {
                mList= new ArrayList<BusRouteBean>();
                JSONObject jsonObject= new JSONObject(response.toString());
                if(jsonObject.has("routes")){
                    JSONArray routesArray= new JSONArray(jsonObject.getString("routes"));
                 //   Log.d("RoutesArray",routesArray.length()+"");
                    for(int i=0;i<routesArray.length();i++){
                        JSONObject routesObj= new JSONObject(String.valueOf(routesArray.get(i)));
                        BusRouteBean bean= new BusRouteBean();
                        if(routesObj!=null && routesObj.has("name")){
                           bean.setStr_BusRoute(routesObj.getString("name"));
                     //       Log.d(REQUEST_TAG,routesObj.getString("name"));
                        }
                        if(routesObj!=null && routesObj.has("accessible")){
                            bean.setBool_Accessible(Boolean.parseBoolean(routesObj.getString("accessible")));
                       //     Log.d(REQUEST_TAG,routesObj.getString("accessible"));
                        }
                        if(routesObj!=null && routesObj.has("image")){
                            bean.setStr_ImageURL(routesObj.getString("image"));
                         //   Log.d(REQUEST_TAG,routesObj.getString("image"));
                        }
                        if(routesObj!=null && routesObj.has("description")){
                            bean.setStr_Desc(routesObj.getString("description"));
                           // Log.d(REQUEST_TAG,routesObj.getString("description"));
                        }
                        if(routesObj!=null && routesObj.has("id")){
                            bean.setStr_ID(routesObj.getString("id"));
                            //Log.d(REQUEST_TAG,routesObj.getString("id"));
                        }
                        if(routesObj!=null && routesObj.has("stops")){
                            JSONArray stopsArray= new JSONArray(routesObj.getString("stops"));
                            String[] stops= new String[stopsArray.length()];
                            for(int j=0;j<stopsArray.length();j++){
                                JSONObject stopObj= new JSONObject(String.valueOf(stopsArray.get(j)));
                                stops[j]=stopObj.getString("name");
                            }
                            bean.setStrA_Stops(stops);
                        }
                        mList.add(bean);
                    }
                    busRouteAdapter= new BusRouteAdapter(getApplicationContext(),mList);
                    rv_BusRoute.setAdapter(busRouteAdapter);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
