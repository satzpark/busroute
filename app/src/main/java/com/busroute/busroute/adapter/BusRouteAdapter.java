package com.busroute.busroute.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.busroute.busroute.R;
import com.busroute.busroute.activities.RouteView;
import com.busroute.busroute.bean.BusRouteBean;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Bewo Technologies on 10/23/2017.
 */

public class BusRouteAdapter extends RecyclerView.Adapter<BusRouteAdapter.MyViewHolder> {
    Context mContext;
    List<BusRouteBean> mList;
    public BusRouteAdapter(Context mContext, List<BusRouteBean> mList) {
        this.mContext=mContext;
        this.mList=mList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder viewHolder = null;
        if(viewType == 1){
            View layoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.busrouteview, parent, false);
            viewHolder = new MyViewHolder(layoutView);
        }else{
            View layoutView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.busrouteview, parent, false);
            viewHolder = new MyViewHolder(layoutView);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BusRouteBean busRouteBean= mList.get(position);

        Picasso.with(mContext).load(busRouteBean.getStr_ImageURL()).into(holder.iv_BusRoute);
        holder.tv_BusRoute.setText(busRouteBean.getStr_BusRoute());


        Log.d("Adapter", busRouteBean.getStr_BusRoute());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tv_BusRoute;
        ImageView iv_BusRoute;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv_BusRoute= (TextView) itemView.findViewById(R.id.tv_BusRoute);
            iv_BusRoute=(ImageView)itemView.findViewById(R.id.iv_BusRoute);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent routeDetails= new Intent(mContext,RouteView.class);
                    routeDetails.putExtra("busroute", getAdapterPosition());
                    routeDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(routeDetails);
                }
            });
        }
    }

}
